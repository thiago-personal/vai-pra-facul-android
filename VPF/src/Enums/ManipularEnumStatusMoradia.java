package Enums;

public class ManipularEnumStatusMoradia {
	public static EnumStatusMoradia IntParaEnumStatusMoradia(int valor){
		switch (valor) {
		case 1:
			return EnumStatusMoradia.ABERTA;
		case 2:
			return EnumStatusMoradia.FECHADA;
		default:
			return null;
		}
	}
}
