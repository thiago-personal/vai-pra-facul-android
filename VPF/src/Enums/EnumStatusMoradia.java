package Enums;

public enum EnumStatusMoradia {
	ABERTA(1), FECHADA(2);
	
	public int valorStatusMoradia;
	
	EnumStatusMoradia(int valor){
		valorStatusMoradia = valor; 
	}
	
	public int getValorStatusMoradia(){
		return valorStatusMoradia;
	}
}
