package br.com.vpf.util;

import java.io.InputStream;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import android.util.Log;
//import java.io.OutputStream;
//import java.net.HttpURLConnection;
//import java.net.URL;

public class OperacoesHTTP {
    public static String[] GET(String url){
        InputStream inputStream = null;
        String[] resposta = new String[2];
        try {
            // create HttpClient
            HttpClient httpclient = new DefaultHttpClient();
            
            HttpGet get = new HttpGet(url);
            get.setHeader("Authorization", Utilidades.CodificarUsuarioSenha());
            // make GET request to the given URL
            HttpResponse httpResponse = httpclient.execute(get);
            StatusLine status = httpResponse.getStatusLine();
            
            inputStream = httpResponse.getEntity().getContent();
            
            if(status.getStatusCode() == 401 || status.getStatusCode() == 403){
            	resposta[0] = null;
            }else if(inputStream == null){
            	resposta[0] = "FALSE";
            }else {
            	resposta[0] = Utilidades.ConverterInputStreamParaString(inputStream);
            }
            
        	resposta[1] = String.valueOf(status.getStatusCode());
            
        	return resposta;
 
        } catch (Exception e) {
            Log.d("InputStream", e.getLocalizedMessage());
            return null;
        }
    }
    
    public static String[] POST(String valor, String urlRequisicao){
    	try {   
    		InputStream inputStream = null;
            String resultado = "";
            HttpClient httpclient = new DefaultHttpClient();
     
            HttpPost httpPost = new HttpPost(urlRequisicao);

            StringEntity se = new StringEntity(valor);
 
            httpPost.setEntity(se);

            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-type", "application/json");

            HttpResponse httpResponse = httpclient.execute(httpPost);
 
            inputStream = httpResponse.getEntity().getContent();
            StatusLine status = httpResponse.getStatusLine();
            
            if(inputStream != null)
            	resultado = Utilidades.ConverterInputStreamParaString(inputStream);
            else
            	resultado = "ERRO";
            
            String[] retorno = new String[2];
            retorno[0] = resultado;
            retorno[1] = String.valueOf(status.getStatusCode());
            
            return retorno;
    	  } catch (Exception e) {  
    	    e.printStackTrace();  
    	    return null; 
    	  }  
    }
}
