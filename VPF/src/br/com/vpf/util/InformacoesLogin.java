package br.com.vpf.util;

import java.util.List;

import br.com.vpf.models.*;

public class InformacoesLogin {
	private static String Email = null;
	private static String Senha = null;
	private static List<RetornoMoradiaSimples> moradias = null;

	public static String getSenha() {
		return InformacoesLogin.Senha;
	}

	public static void setSenha(String senha) {
		//Ter� que conter as regras para uma senha
		InformacoesLogin.Senha = senha;
	}

	public static String getEmail() {
		return InformacoesLogin.Email;
	}

	public static void setEmail(String email) {
		//Ter� que conter as regras para o email
		InformacoesLogin.Email = email;
	}

	public static List<RetornoMoradiaSimples> getMoradias() {
		return moradias;
	}

	public static void setMoradias(List<RetornoMoradiaSimples> moradias) {
		InformacoesLogin.moradias = moradias;
	}
	
}
