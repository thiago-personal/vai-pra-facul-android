package br.com.vpf.util;

import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import Enums.EnumStatusMoradia;
import Enums.ManipularEnumStatusMoradia;
import br.com.vpf.models.*;

public class TraduzirJson {
	public static List<RetornoMoradiaSimples> JsonParaRetornoMoradiaSimples(JSONArray valoresJson){
		int tamanho = valoresJson.length();
		List<RetornoMoradiaSimples> moradiaSimples = new ArrayList<RetornoMoradiaSimples>();
		try {
			for (int i = 0; i < tamanho; i++) {
				JSONObject jsonObjetoValor;
				jsonObjetoValor = valoresJson.getJSONObject(i);
		        RetornoMoradiaSimples retornoMoradiaAuxiliar = new RetornoMoradiaSimples();
		        retornoMoradiaAuxiliar.setId(jsonObjetoValor.getInt("Id"));
		        retornoMoradiaAuxiliar.setNome(jsonObjetoValor.getString("Nome"));
		        retornoMoradiaAuxiliar.setPreco(jsonObjetoValor.getDouble("Preco"));
		        retornoMoradiaAuxiliar.setFaculdade(jsonObjetoValor.getString("Faculdade"));
		        EnumStatusMoradia statusMoradia = ManipularEnumStatusMoradia.IntParaEnumStatusMoradia(jsonObjetoValor.getInt("Status"));
		        retornoMoradiaAuxiliar.setStatusMoradia(statusMoradia);		
		        moradiaSimples.add(retornoMoradiaAuxiliar);				
			}
			return moradiaSimples;
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
	}
}
