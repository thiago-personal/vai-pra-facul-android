package br.com.vpf.util;

import br.com.vpf.models.CadastrarUsuario;
import com.google.gson.Gson;

public class EscreverObjetoJson {
	public static String EscreverUsuario(CadastrarUsuario cadastrarUsuario){
		Gson gson = new Gson();
		return gson.toJson(cadastrarUsuario);
	}
}
