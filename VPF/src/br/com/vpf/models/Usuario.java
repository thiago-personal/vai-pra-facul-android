package br.com.vpf.models;

public class Usuario {
	private String Email = null;
	private String Nome = null;
	private String Sobrenome = null; 
	private String DataNascimento = null;
	private int Sexo = -1;
	private String Telefone = null;
	private Byte[] Imagem = null;
	
	public Usuario(){
	}
	
	public Usuario(String email, String nome, String sobrenome, String dataNascimento, int sexo, String telefone, Byte[] imagem){
		this.setEmail(email);
		this.setNome(nome);
		this.setSobrenome(sobrenome);
		this.setDataNascimento(dataNascimento);
		this.setSexo(sexo);
		this.setTelefone(telefone);
		this.setImagem(imagem);
	}
	
	public String getEmail() {
		return Email;
	}
	public void setEmail(String email) {
		Email = email;
	}
	public String getNome() {
		return Nome;
	}
	public void setNome(String nome) {
		Nome = nome;
	}
	public String getSobrenome() {
		return Sobrenome;
	}
	public void setSobrenome(String sobrenome) {
		Sobrenome = sobrenome;
	}
	public String getDataNascimento() {
		return DataNascimento;
	}
	public void setDataNascimento(String dataNascimento) {
		DataNascimento = dataNascimento;
	}
	public int getSexo() {
		return Sexo;
	}
	public void setSexo(int sexo) {
		Sexo = sexo;
	}
	public String getTelefone() {
		return Telefone;
	}
	public void setTelefone(String telefone) {
		Telefone = telefone;
	}
	public Byte[] getImagem() {
		return Imagem;
	}
	public void setImagem(Byte[] imagem) {
		Imagem = imagem;
	}
}
