package br.com.vpf.models;

import Enums.*;

public class RetornoMoradiaSimples {
    private Integer Id;
    private String Nome;
    private Double Preco;
    private String Faculdade;
    private EnumStatusMoradia StatusMoradia;
    
    public RetornoMoradiaSimples(){
    }
    
    public RetornoMoradiaSimples(Integer id, String nome, Double preco, String faculdade, EnumStatusMoradia statusMoradia){
    	this.setId(id);
    	this.setNome(nome);
    	this.setPreco(preco);
    	this.setFaculdade(faculdade);
    	this.setStatusMoradia(statusMoradia);
    }
    
	public Integer getId() {
		return Id;
	}
	public void setId(Integer id) {
		Id = id;
	}
	public String getNome() {
		return Nome;
	}
	public void setNome(String nome) {
		Nome = nome;
	}
	public Double getPreco() {
		return Preco;
	}
	public void setPreco(Double preco) {
		Preco = preco;
	}
	public String getFaculdade() {
		return Faculdade;
	}
	public void setFaculdade(String faculdade) {
		Faculdade = faculdade;
	}
	public Enums.EnumStatusMoradia getStatusMoradia() {
		return StatusMoradia;
	}
	public void setStatusMoradia(Enums.EnumStatusMoradia statusMoradia) {
		StatusMoradia = statusMoradia;
	}
}
