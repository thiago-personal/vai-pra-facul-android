package br.com.vpf.models;

public class CadastrarUsuario {
	private Usuario InformacoesUsuario = null;
    private String Senha = null;
    private String ConfirmarSenha = null;
    
    public CadastrarUsuario(){
    }
    
    public CadastrarUsuario(Usuario informacoesUsuario, String senha, String confirmarSenha){
    	this.setInformacoesUsuario(informacoesUsuario);
    	this.setSenha(senha);
    	this.setConfirmarSenha(confirmarSenha);
    }
    
	public Usuario getInformacoesUsuario() {
		return InformacoesUsuario;
	}
	public void setInformacoesUsuario(Usuario informacoesUsuario) {
		InformacoesUsuario = informacoesUsuario;
	}
	public String getSenha() {
		return Senha;
	}
	public void setSenha(String senha) {
		Senha = senha;
	}
	public String getConfirmarSenha() {
		return ConfirmarSenha;
	}
	public void setConfirmarSenha(String confirmarSenha) {
		ConfirmarSenha = confirmarSenha;
	}
}
