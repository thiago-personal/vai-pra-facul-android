package br.com.vpf.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

public class SexoAdapter extends BaseExpandableListAdapter {
	private Context Contexto;
	String[] ListaSexo = {"Sexo"};
	String[][] ListaSexoFilho = {{"Feminino", "Masculino"}};
	
	public SexoAdapter(Context contexto){
		this.setContexto(contexto);
	}
	
	@Override
	public Object getChild(int posicaoGrupo, int posicaoFilho) {
		return ListaSexoFilho[posicaoGrupo][posicaoFilho];
	}

	@Override
	public long getChildId(int posicaoGrupo, int posicaoFilho) {
		// TODO Auto-generated method stub
		return posicaoFilho;
	}

	@Override
	public View getChildView(int posicaoGrupo, int posicaoFilho, boolean eUltimoFilho, View converterView,
			ViewGroup pai) {
		CheckBox checkBox = new CheckBox(this.getContexto());
		//TextView textViewSubLista = new TextView(this.getContexto());
		checkBox.setText(this.ListaSexoFilho[posicaoGrupo][posicaoFilho]);
		//textViewSubLista.setText(this.ListaSexoFilho[posicaoGrupo][posicaoFilho]);
		checkBox.setPadding(10, 5, 0, 5);
		//textViewSubLista.setPadding(10, 5, 0, 5);
		
		return checkBox;
		//return textViewSubLista;
	}

	@Override
	public int getChildrenCount(int posicaoGrupo) {
		// TODO Auto-generated method stub
		return this.ListaSexoFilho[posicaoGrupo].length;
	}

	@Override
	public Object getGroup(int posicao) {
		// TODO Auto-generated method stub
		return this.ListaSexo[posicao];
	}

	@Override
	public int getGroupCount() {
		// TODO Auto-generated method stub
		return this.ListaSexo.length;
	}

	@Override
	public long getGroupId(int posicao) {
		// TODO Auto-generated method stub
		return posicao;
	}

	@Override
	public View getGroupView(int posicaoGrupo, boolean expande, View converterView, ViewGroup pai) {
		TextView textViewCategorias = new TextView(this.getContexto());
		textViewCategorias.setText(this.ListaSexo[posicaoGrupo]);
		textViewCategorias.setPadding(30, 5, 0, 5);
		textViewCategorias.setTextSize(20);
		textViewCategorias.setTypeface(null, Typeface.BOLD);
		
		return textViewCategorias;
	}

	@Override
	public boolean hasStableIds() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isChildSelectable(int arg0, int arg1) {
		// TODO Auto-generated method stub
		return false;
	}

	public Context getContexto() {
		return Contexto;
	}

	public void setContexto(Context contexto) {
		Contexto = contexto;
	}

}
