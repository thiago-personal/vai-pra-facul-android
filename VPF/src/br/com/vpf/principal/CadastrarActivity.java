package br.com.vpf.principal;

import br.com.vpf.adapters.SexoAdapter;
import br.com.vpf.models.CadastrarUsuario;
import br.com.vpf.models.Usuario;
import br.com.vpf.util.EscreverObjetoJson;
import br.com.vpf.util.OperacoesHTTP;
import br.com.vpf.util.URLS;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.TextView;
import android.widget.Toast;

public class CadastrarActivity extends Activity {

	ExpandableListView dblSexo;
	Context contexto;
	Button btnEnviar;
	EditText etEmail, etNome, etSobrenome, etDataNascimento, etTelefone, etSenha, etConfirmarSenha;
	CadastrarUsuario cadastrarUsuario;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_cadastrar);
		contexto = this;
		dblSexo = (ExpandableListView) findViewById(R.id.cadastrar_dblSexo);
		btnEnviar = (Button) findViewById(R.id.cadastrar_btnEnviar);
		etEmail = (EditText) findViewById(R.id.cadastrar_etEmail);
		etNome = (EditText) findViewById(R.id.cadastrar_etNome);
		etSobrenome = (EditText) findViewById(R.id.cadastrar_etSobrenome);
		etDataNascimento = (EditText) findViewById(R.id.cadastrar_etDataNascimento);
		etTelefone = (EditText) findViewById(R.id.cadastrar_etTelefone);
		etSenha = (EditText) findViewById(R.id.cadastrar_etSenha);
		etConfirmarSenha = (EditText) findViewById(R.id.cadastrar_etConfirmarSenha);
		
		dblSexo.setAdapter(new SexoAdapter(contexto));
		new CadastrarUsuarioTask().execute((Void) null);
		btnEnviar.setOnClickListener(
			new View.OnClickListener(){
				@Override
				public void onClick(View view) {
					Usuario usuario = new Usuario();
					//Toast.makeText(getBaseContext(), etEmail.getText().toString(), Toast.LENGTH_LONG).show();
					/*usuario.setEmail(etEmail.getText().toString());
					usuario.setNome(etNome.getText().toString());
					usuario.setSobrenome(etSobrenome.getText().toString());
					usuario.setDataNascimento(etDataNascimento.getText().toString());
					
					cadastrarUsuario = new CadastrarUsuario();
					cadastrarUsuario.setSenha(etSenha.getText().toString());
					cadastrarUsuario.setConfirmarSenha(etConfirmarSenha.getText().toString());
					cadastrarUsuario.setInformacoesUsuario(usuario); */
					new CadastrarUsuarioTask().execute((Void) null);
				}
			});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.cadastrar, menu);
		return true;
	}
	
	public class CadastrarUsuarioTask extends AsyncTask<Void, Void, String[]> {
		@Override
		protected String[] doInBackground(Void... params) {
			//Substituir esses valores pelo que o usu�rio digitar na tela de cadastro
			Usuario usuario = new Usuario("hahaha.com.br", "haha", "huhu", "24-12-1992", 1, "11-6038-5868", null);
			CadastrarUsuario cadastrarUsuario1 = new CadastrarUsuario(usuario, "123456", "123456");
			String valor = EscreverObjetoJson.EscreverUsuario(cadastrarUsuario1);
			return OperacoesHTTP.POST(valor, URLS.CadastrarUsuario);
		}
		@Override
		protected void onPostExecute(String[] resposta) {
			Toast.makeText(getBaseContext(), ""+ resposta[0] +" E Status: "+resposta[1]+"", Toast.LENGTH_LONG).show();
		}
	}

}
