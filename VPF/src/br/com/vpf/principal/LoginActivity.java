package br.com.vpf.principal;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import br.com.vpf.models.RetornoMoradiaSimples;
import br.com.vpf.util.InformacoesLogin;
import br.com.vpf.util.OperacoesHTTP;
import br.com.vpf.util.TraduzirJson;
import br.com.vpf.util.URLS;
import br.com.vpf.util.Utilidades;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Activity which displays a login screen to the user, offering registration as
 * well.
 */
public class LoginActivity extends Activity {

	private UserLoginTask mAuthTask = null;

	// Values for email and password at the time of the login attempt.
	private String Email;
	private String Senha;

	// UI references.
	private EditText etEmail;
	private EditText etSenha;
	private TextView etCadastrarAqui;
	private View mLoginFormView;
	private View mLoginStatusView;
	private TextView mLoginStatusMessageView;
	private Context contexto;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_login);
		
		Bitmap teste = Utilidades.BaixarImagem("http://10.205.19.176:50934/api/Moradia/haha/12");
		ImageView user_image = (ImageView) findViewById(R.id.imgViewTeste);
		user_image.setImageBitmap(teste);
		// Set up the login form.
		//mEmail = getIntent().getStringExtra(EXTRA_EMAIL);
		etEmail = (EditText) findViewById(R.id.cadastrar_etTelefone);
		//mEmailView.setText(mEmail);
		contexto = this; 
		
		etSenha = (EditText) findViewById(R.id.etSenha);
		etSenha.setOnEditorActionListener(new TextView.OnEditorActionListener() {
					@Override
					public boolean onEditorAction(TextView textView, int id,
							KeyEvent keyEvent) {
						if (id == R.id.login || id == EditorInfo.IME_NULL) {
							attemptLogin();
							return true;
						}
						return false;
					}
				});
		
		etCadastrarAqui = (TextView) findViewById(R.id.tvCadastrarUsuarioComum);
		etCadastrarAqui.setOnClickListener(
				new View.OnClickListener() {
					@Override
					public void onClick(View view) {
						Intent cadastrar = new Intent(contexto, CadastrarActivity.class);
						startActivity(cadastrar);
					}
		});

		mLoginFormView = findViewById(R.id.login_form);
		mLoginStatusView = findViewById(R.id.login_status);
		mLoginStatusMessageView = (TextView) findViewById(R.id.login_status_message);

		findViewById(R.id.btnLogin).setOnClickListener(
				new View.OnClickListener() {
					@Override
					public void onClick(View view) {
						attemptLogin();
					}
				});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		getMenuInflater().inflate(R.menu.login, menu);
		return true;
	}

	/**
	 * Attempts to sign in or register the account specified by the login form.
	 * If there are form errors (invalid email, missing fields, etc.), the
	 * errors are presented and no actual login attempt is made.
	 */
	public void attemptLogin() {
		if (mAuthTask != null) {
			return;
		}

		// Reset errors.
		etEmail.setError(null);
		etSenha.setError(null);

		// Store values at the time of the login attempt.
		Email = etEmail.getText().toString();
		Senha = etSenha.getText().toString();

		boolean cancel = false;
		View focusView = null;

		// Check for a valid password.
		if (TextUtils.isEmpty(Senha)) {
			etSenha.setError(getString(R.string.action_esqueci_senha));
			focusView = etSenha;
			cancel = true;
		} else if (Senha.length() < 6) {
			etSenha.setError(getString(R.string.erro_campo_obrigatorio));
			focusView = etSenha;
			cancel = true;
		}

		// Check for a valid email address.
		if (TextUtils.isEmpty(Email)) {
			etEmail.setError(getString(R.string.erro_campo_obrigatorio));
			focusView = etEmail;
			cancel = true;
		} else if (!Email.contains("@")) {
			etEmail.setError(getString(R.string.erro_email_invalido));
			focusView = etEmail;
			cancel = true;
		}

		if (cancel) {
			// There was an error; don't attempt login and focus the first
			// form field with an error.
			focusView.requestFocus();
		} else {
			// Show a progress spinner, and kick off a background task to
			// perform the user login attempt.
			mLoginStatusMessageView.setText(R.string.login_progresso_logando);
			showProgress(true);
			mAuthTask = new UserLoginTask();
			mAuthTask.execute((Void) null);
		}
	}

	/**
	 * Shows the progress UI and hides the login form.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
	private void showProgress(final boolean show) {
		// On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
		// for very easy animations. If available, use these APIs to fade-in
		// the progress spinner.
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
			int shortAnimTime = getResources().getInteger(
					android.R.integer.config_shortAnimTime);

			mLoginStatusView.setVisibility(View.VISIBLE);
			mLoginStatusView.animate().setDuration(shortAnimTime)
					.alpha(show ? 1 : 0)
					.setListener(new AnimatorListenerAdapter() {
						@Override
						public void onAnimationEnd(Animator animation) {
							mLoginStatusView.setVisibility(show ? View.VISIBLE
									: View.GONE);
						}
					});

			mLoginFormView.setVisibility(View.VISIBLE);
			mLoginFormView.animate().setDuration(shortAnimTime)
					.alpha(show ? 0 : 1)
					.setListener(new AnimatorListenerAdapter() {
						@Override
						public void onAnimationEnd(Animator animation) {
							mLoginFormView.setVisibility(show ? View.GONE
									: View.VISIBLE);
						}
					});
		} else {
			// The ViewPropertyAnimator APIs are not available, so simply show
			// and hide the relevant UI components.
			mLoginStatusView.setVisibility(show ? View.VISIBLE : View.GONE);
			mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
		}
	}

	/**
	 * Represents an asynchronous login/registration task used to authenticate
	 * the user.
	 */
	public class UserLoginTask extends AsyncTask<Void, Void, String[]> {
		@Override
		protected String[] doInBackground(Void... params) {
			InformacoesLogin.setEmail(Email);
			InformacoesLogin.setSenha(Senha);
			
			return OperacoesHTTP.GET(URLS.Login);
		}
		@Override
		protected void onPostExecute(String[] resultado) {
			mAuthTask = null;
			showProgress(false);
			Integer codigoHTTP = Integer.parseInt(resultado[1]);
			
			if(codigoHTTP == 401){
				//Mensagem que usu�rio ou senha est� errado ou usu�rio n�o cadastrado
				etSenha.setError(getString(R.string.erro_emailousenha_incorreta));
				etSenha.requestFocus();
			}else if (codigoHTTP == 403) {
				//Mensagem que n�o tem autoriza��o para acessar essa funcionalidade
				Toast.makeText(getBaseContext(), "N�o tem autoriza��o!", Toast.LENGTH_LONG).show();
			}else if(codigoHTTP == 500){
				Toast.makeText(getBaseContext(), "Erro do servidor!", Toast.LENGTH_LONG).show();
			}else if(resultado[0].equals("FALSE")){
				Toast.makeText(getBaseContext(), "Erro com conex�o!", Toast.LENGTH_LONG).show();
			}else {				
				//JSONObject valoresJson;
				JSONArray valoresJson;
				try {
					valoresJson = new JSONArray(resultado[0]);
					InformacoesLogin.setMoradias(new ArrayList<RetornoMoradiaSimples>());
					InformacoesLogin.setMoradias(TraduzirJson.JsonParaRetornoMoradiaSimples(valoresJson));
				
			        /*String nome = "";
			        for(int i = 0; i < InformacoesLogin.getMoradias().size(); i++){
			        	nome  += (InformacoesLogin.getMoradias().get(i).getNome() + "\n");
			        }*/
			        
			        //Toast.makeText(getBaseContext(), nome, Toast.LENGTH_LONG).show();
					Intent inicialLogado = new Intent(contexto, MainActivity.class);
					startActivity(inicialLogado);
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}

		@Override
		protected void onCancelled() {
			mAuthTask = null;
			showProgress(false);
		}
	}
}
