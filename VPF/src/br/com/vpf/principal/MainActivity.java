package br.com.vpf.principal;

import java.util.List;

import br.com.vpf.models.RetornoMoradiaSimples;
import br.com.vpf.util.InformacoesLogin;
import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {
	
	Context contexto;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        /*String nome = "";
        for(int i = 0; i < InformacoesLogin.getMoradias().size(); i++){
        	nome  += (InformacoesLogin.getMoradias().get(i).getNome() + "\n");
        }
        
        Toast.makeText(getBaseContext(), nome, Toast.LENGTH_LONG).show();*/
        PreencherLista();
        clicarItemDaLista();
    }

    
    public void PreencherLista(){
    	ArrayAdapter<RetornoMoradiaSimples> adapter = new MoradiaInicialAdapter();
    	ListView lista = (ListView) findViewById(R.id.lvMoradiasInicial);
    	lista.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    
	private void clicarItemDaLista() {
		ListView lista = (ListView) findViewById(R.id.lvMoradiasInicial);
		lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View viewClicked,
					int position, long id) {
				
				RetornoMoradiaSimples moradia = InformacoesLogin.getMoradias().get(position);
				String mensagem = "Vc clicou na posi��o " + position
								+ " Que � a Moradia:  " + moradia.getNome();
				Toast.makeText(MainActivity.this, mensagem, Toast.LENGTH_LONG).show();
			}
		});
	}
    
    private class MoradiaInicialAdapter  extends ArrayAdapter<RetornoMoradiaSimples>{    	
    	public MoradiaInicialAdapter() {
    		super(MainActivity.this, R.layout.item_view_moradias_inicial, InformacoesLogin.getMoradias());
    	}
    	
    	@Override
    	public View getView(int position, View convertView, ViewGroup parent) {
    		View itemView = convertView;
    		if(itemView == null){
    			itemView = getLayoutInflater().inflate(R.layout.item_view_moradias_inicial, parent, false);
    		}

    		RetornoMoradiaSimples moradia = InformacoesLogin.getMoradias().get(position);
    		
    		TextView teste1 = (TextView) itemView.findViewById(R.id.tvTeste1);
    		teste1.setText(moradia.getNome());
    		
    		TextView teste2 = (TextView) itemView.findViewById(R.id.tvTeste2);
    		teste2.setText(moradia.getFaculdade());

    		return itemView;
    	}
    }
    
}
